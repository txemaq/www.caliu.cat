+++
title = "Compte bancari"
date = "2021-10-17"
+++

# Canvi d’entitat bancaria

Des de la fundació de CALIU s’havia confiat la tresoreria a una caixa d’arrel catalana. Arribats a un punt, el cost que representava per l’associació, comparat amb l’ús que en fèiem, va fer decidir el canvi d’entitat a l’assemblea del gener de 2013.

De les diferents opcions ens va semblar que, una organització com la nostra, havia de triar una entitat que oferís una base ètica a la seva operativa.

A la tardor del 2019, i degut a l’augment de les comissions a Triodos, es va obrir un compte a la **Caixa Rural de Guissona**, una entitat catalana que actualment no cobra comissions per tenir diners al compte. El número IBAN del compte bancari actual és:

**ES06 3140 0001 9100 1420 6000**

Per qualsevol dubte o qüestió us podeu adreçar a la [Junta de Caliu](mailto:junta(eliminatotaixò)@caliu.cat).
