+++
title = "Llistes de correu"
date = "2021-10-17"
+++

Disposem ara mateix de 2 llistes de correu:

[caliu-info](https://lists.riseup.net/www/info/caliu-info)

> Llista genèrica per a tot el que necessiteu. Si teniu qualsevol altre dubte sobre el funcionament del GNU/Linux és aquí on heu de preguntar-ho. Podeu veure’n&nbsp;<a href="https://lists.riseup.net/www/arc/caliu-info" target="_blank" rel="noopener">l’històric de missatges</a>.

[caliu-esdeveniments](https://lists.riseup.net/www/info/caliu-esdeveniments)

> Llista d’anunci d’esdeveniments als territoris de parla catalana. Podeu veure’n [l’històric de missatges](https://lists.riseup.net/www/arc/caliu-esdeveniments)
