+++
title = "Rèpliques al servidor web"
date = "2021-10-17"
+++

En una aposta per donar un servei de qualitat a la comunitat GNU/LINUX de l'àmbit català, del nostre servidor d'HTTP us podeu descarregar directament la vostra distribució preferida, gràcies a les rèpliques actualitzades diàriament.

Quant als documents disponibles des d'aquest servidor, ni Caliu ni els seus membres donen cap garantia ni assumeix cap responsabilitat legal pel contingut de les imatges ISO allotjades al servidor.

> Nota: antigament hi havia un servei de FTP que ja no tenim. Ara es pot accedir als mateixos continguts però només per http o https.

## Política dels miralls de Caliu
* Un dels objectius de Caliu és la difusió i el suport de GNU/Linux en llengua catalana i una de les maneres de fer-ho és hostatjar miralls de distribucions.
* Prioritzem distribucions locals (properes a la comunitat lingüística catalana), que disposin de traducció al català, i que siguin de programari lliure.
* Oferim el servei condicionat als recursos físics i humans disponibles. Això ens pot fer descartar distribucions o parts de distribucions segons criteris d'ocupació de disc, ample de banda per a actualitzacions o esforç de manteniment.
* Es podran tenir en compte els criteris de popularitat i d'interès històric a l'hora d'excloure o preservar els continguts dels miralls.
* Les distribucions no poden contenir fitxers la llicència dels quals no permetin distribuir-los.
    * La intenció és d'una banda no buscar-nos problemes legals ni nosaltres ni qui ens cedeix infraestructura, però també evitar-los a qui vulgui fer una rèplica de les nostres rèpliques o passar continguts a un veí en un suport. Això no vol dir que donem cap garantia de res, comprovar la legalitat o no de cada detall de cada distribució en cada jurisdicció és una feina enorme que no fem nosaltres. Nosaltres fem confiança a la gent que treballa fent distribucions que una de les feines que fan és seleccionar material que sigui redistribuïble. Simplement no pengem res si d'entrada ja dubtem que sigui legal posar-ho, tret que hi ha hagi molts bons motius per a la desobediència (fomentar el programari privatiu no és un bon motiu).
    * Per exemple: algunes distribucions inclouen el connector del Flash. Altres no porten el programa però porten un instal·lador que si l'usuari l'activa es baixa el programa directament dels servidors d'Adobe. Les primeres necessiten un acord explícit amb Adobe, perquè la llicència d'ús del connector de flash no permet la redistribució. Caliu té altra feina que buscar acords per redistribuir programari privatiu, i no vol induir a error els usuaris, per tant les distribucions que sabem que porten directament el programa ja no les pengem.

Podeu consultar el [ssh_known_hosts](http://ftp.caliu.cat/pub/caliu/other/known_hosts.caliu.cat) de Caliu juntament amb les [signatures GPG](http://ftp.caliu.cat/pub/caliu/other/known_hosts.caliu.cat.sig) per assegurar-vos que no aneu a parar on no toca.

Si teniu algun suggeriment o comentari, poseu-vos en contacte amb la [junta](mailto:junta_a_caliu.cat). En cas que vulgueu demanar un mirall nou, ens serà útil que ens indiqueu les dades següents:
* Tipus de mirall (rsync, ftpsync, etc).
* Mida total.
* Freqüència d'actualització.
* Llicències de distribució.
* Qualsevol altra dada que considereu important.

 
### Distribucions
[Debian](https://ftp.caliu.cat/debian/) | [Debian-CD](https://ftp.caliu.cat/debian-cd/)  
[Gentoo](https://ftp.caliu.cat/gentoo/)  
[Manjaro](https://ftp.caliu.cat/manjaro/)  
[Ubuntu](https://ftp.caliu.cat/ubuntu/) | [Ubuntu-CD](https://ftp.caliu.cat/ubuntu-cd/)  
[Linkat](https://ftp.caliu.cat/pub/distribucions/linkat/)  
[Trisquel](https://ftp.caliu.cat/pub/distribucions/trisquel/)

### Distros antigues, museu històric
[Yellow Dog](http://ftp.caliu.cat/pub/distribucions/yellowdog/)  
[Catix](http://ftp.caliu.cat/pub/distribucions/catix/)  
[Knoppix](http://ftp.caliu.cat/pub/distribucions/knoppix/)  
[K-DEmar](http://ftp.caliu.cat/pub/distribucions/k-demar/)
