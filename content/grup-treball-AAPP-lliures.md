+++
title = "Grup de treball per unes AAPP lliures"
date = "2021-12-05"
+++

# Introducció

Entre el setembre i l'octubre de 2020, a la [llista de correu general de Caliu](https://lists.riseup.net/www/arc/caliu-info) es va comentar i debatre l'existència de formularis PDF per fer tràmits en línia a la web de la Generalitat de Catalunya ([Gencat](https://web.gencat.cat/)) i que donaven problemes en obrir-los amb el programari existent des del sistema operatiu (SO) Linux. 
 
A partir d'aquí, Caliu va decidir crear un grup de treball per analitzar en profunditat el problema dels tràmits en línia de la Generalitat de Catalunya (Gencat) i cercar alguna solució per a que es poguessin fer des del sistema operatiu Linux.
 
 
# Grup de treball 
 
El gener de 2021 es crea el grup de treball, amb l'objectiu que la Gencat accepti el SO Linux per a fer qualsevol tràmit en línia.


## Primera fase: Recollida de tràmits no disponibles amb Linux

El grup de treball va començar per recopilar exemples de tràmits en línia des de la web de la Gencat que no es podien efectuar amb el SO Linux.

La majoria d'exemples recollits corresponen a tràmits en línia que requereixen omplir formularis PDF que utilitzen extensions XFA, les quals no formen part de l'estàndard PDF, i que per tant, no estan implementades en el programari Linux de gestió de documents PDF.

També hi ha altres exemples amb documents en format «.docx» d'una [agència de la Gencat](https://www.aqu.cat/) i altres administracions públiques que no es poden obrir amb el programari ofimàtic des de Linux.


## Segona fase: Anàlisi de la normativa que obliga a les administracions a donar un serveix interoperable

Hi ha diversa normativa, tant europea com estatal, que regula l'ús del programari i dels estàndards a les administracions públiques i en les seves relacions amb els ciutadans. Destaquem la normativa següent:

-   El RD 203/2021, de 30 de març, pel que s'aprova el «Reglamento de actuación y funcionamiento del    sector público por medios electrónicos», a l'article 2n apartat «a» s'indica que el sector públic respectarà els principis de neutralitat tecnològica i d'adaptabilitat (...) per a garantir tant la independència en l'elecció de les alternatives tecnològiques necessàries per a relacionar-se amb les Administracions Públiques per part de les persones interessades (...). A aquests efectes, el sector públic utilitzarà estàndards oberts, així com, en el seu cas i de forma complementaria, estàndards que siguin d'ús generalitzat. 

-   Resolución de 3 de octubre de 2012 (BOE 31 de octubre) de la Secretaría de Estado de Administraciones Públicas, por la que se aprueba la Norma Técnica de Interoperabilidad de Catálogo de estándares. Veure al 
[BOE](https://www.boe.es/buscar/act.php?id=BOE-A-2012-13501). En aquest document s'indiquen els estàndards que poden utilitzar les administracions públiques.


## Tercera fase: Instància per demanar a la Gencat que permeti els tràmits en línia des del SO Linux

A partir de la documentació recollida i l'anàlisi efectuat de la normativa, s'ha decidit enviar una instància a la Gencat sol·licitant que s'implementin alternatives als tràmits en línia que actualment no es poden fer amb el SO Linux.

En un primer moment només ens hem enfocat en solucionar els problemes amb els formularis PDF amb extensions XFA, ja que és la xacra més estesa en els tràmits en línia. En funció de la resposta de la Gencat, farem una campanya addicional pels altres problemes detectats. 

La instància enviada és la següent (sense les dades sensibles):

```
Nom associació: Associació d'Usuaris i Usuàries de GNU/Linux en Llengua Catalana (CALIU)
Domicili: xxxxxxxxxxxxxxxxxxxx
Adreça de correu: xxxxxxxxxxxxxxxxxxxx
CIF: xxxxxxxxxxxxxxxxxxxx

Representada per: xxxxxxxxxxxxxxxxxxxx
Domicili: xxxxxxxxxxxxxxxxxxxx
Adreça de correu: xxxxxxxxxxxxxxxxxxxx
NIF: xxxxxxxxxxxxxxxxxxxx


EXPOSICIÓ DE FETS:

1. Que segons el RD 203/2021, de 30 de març, pel que s'aprova el «Reglamento de actuación y funcionamiento 
del sector público por medios electrónicos», a l'article 2n, apartat «a» s'indica que el sector públic 
respectarà els principis de neutralitat tecnològica i d'adaptabilitat [...] per a garantir tant la 
independència en l'elecció de les alternatives tecnològiques necessàries per a relacionar-se amb les 
Administracions Públiques per part de les persones interessades [...]. A aquests efectes, el sector públic 
utilitzarà estàndards oberts, així com, en el seu cas i de forma complementaria, estàndards que siguin 
d'ús generalitzat.

2. Que els documents en format PDF (Portable Document Format) estan regulats per l'estàndard ISO 32000, i 
també estan a l'inventari de la «Resolució de 3 d'octubre de 2012 (BOE 31 d'octubre) de la Secretaría de 
Estado de Administraciones Públicas, por la que se aprueba la Norma Técnica de Interoperabilidad de 
Catálogo de estándares.»

3. Que les extensions XFA (XML Forms Architecture) no formen part dels estàndards internacionals, ni 
estan a l'inventari de de la «Resolució de 3 d'octubre de 2012 (BOE 31 d'octubre) de la Secretaría de 
Estado de Administraciones Públicas, por la que se aprueba la Norma Técnica de Interoperabilidad de 
Catálogo de estándares.»

4. Que les extensions XFA es consideren obsoletes des de la versió 2.0 de la definició tècnica de 
l'estàndard PDF, establert a la norma ISO 32000-2 publicada l'any 2017.

5. Que els formularis XFA no estan suportats per cap lector/editor de PDF, excepte les versions 
d'escriptori de l'Adobe Acrobat Reader i l'Adobe Acrobat, només per als sistemes operatius Windows i Mac. 
Aquest fet deixa fora de cobertura tots els altres sistemes operatius d'escriptori, com el Linux, i tots 
els dispositius que empren sistemes operatius per a dispositius mòbils (tauletes, mòbils, etc.), com per 
exemple, l'Android i l'iOS.

6. Que la Generalitat de Catalunya utilitza de manera generalitzada documents PDF amb extensions XFA que 
els ciutadans han d'omplir en els tràmits per Internet en la seva relació amb l'administració, sense una 
alternativa accessible com seria omplir un formulari HTML (HTML5). Hi ha molts tràmits en línia que només 
estan disponibles com a document PDF amb extensió XFA, però a tall d'exemple es poden citar els següents:

6.1) Departament de Treball, Afers Socials i Família. Títol de família nombrosa (Sol·licitar el títol, 
Renovar el títol, Sol·licitar un duplicat). Per la sol·licitud del títol:
 
https://web.gencat.cat/ca/tramits/tramits-temes/Titol-de-familia-nombrosa?category=76531384-a82c-11e3-a972-000c29052e2c&moda=1

6.2) Departament de Justícia. Tràmits de les associacions (Canvi de domicili social, Constitució d'una 
associació, Inscripció dels òrgans de govern d'una associació,...). Per la constitució d'una associació:

https://web.gencat.cat/ca/tramits/tramits-temes/Constitucio-duna-associacio?category=753ac520-a82c-11e3-a972-000c29052e2c&moda=1

6.3) Departament d'Interior. Comunicació de manifestacions o concentracions (Comunicar una manifestació 
o concentració, Comunicar la modificació d'una manifestació o concentració, Comunicar la desconvocatòria 
d'una manifestació o concentració prèviament convocada):

https://web.gencat.cat/ca/tramits/tramits-temes/Comunicacio-de-manifestacions-o-concentracions?category=74534bfa-a82c-11e3-a972-000c29052e2c

6.4) Departament de Territori i Sostenibilitat. Sol·licitud d'autorització d'activitats en espais 
naturals protegits:

https://web.gencat.cat/ca/tramits/tramits-temes/Autoritzacio-dactivitats-en-espais-naturals-protegits?category=22436bbe-9e40-11e9-959c-005056924a59&moda=1


SOL·LICITUD:

Que els diferents tràmits per Internet de la Generalitat de Catalunya es puguin fer amb estàndards oberts, 
com per exemple, els formularis HTML. Això permetria que aquests tràmits es puguin fer per Internet des 
d'ordinadors d'escriptori amb sistema operatiu Linux, i també des dels dispositius mòbils d'ús 
generalitzat com telèfons intel·ligents i tauletes, complint així amb la normativa actual. Cal tenir 
present que la tecnologia de formularis XFA és obsoleta i cada vegada tindrà menys presència digital.

Barcelona, 2 de novembre de 2021

```

Aquesta instància es va enviar a la Generalitat el dia 02/11/2021, mitjançant els tràmits en línia. En concret, s'envià a la Direcció General de Serveis Digitals i Experiència Ciutadana. 

El dia 11/11/2021 l'administració va confirmar que la instància s'havia lliurat a la Direcció General de Serveis Digitals i Experiència Ciutadana, del Departament de la Vicepresidència i de Polítiques Digitals i Territori.


## Quarta fase: Pròxims passos

En funció de la resposta de la Generalitat, s'estudiaran els passos següents.


