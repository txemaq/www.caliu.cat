+++
author = "Alex Muntada"
title = "Estem fent reformes!"
date = "2021-03-12"
tags = [
    "web",
]
+++

Tot just hem començat a repensar alguns dels serveis que teníem fins ara i us trobareu que alguns han deixat de funcionar sota el domini caliu.cat:
<!--more-->

* el blog principal
* els blogs personals
* el wiki
* les pàgines de patents
* el planeta
* les fotos

Continuen funcionant aquests altres serveis:

* ftp.caliu.cat
* status.caliu.cat

Disculpeu les molèsties.
